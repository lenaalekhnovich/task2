package test;

import file.FileWork;
import objects.Chef;
import objects.Salad;
import objects.Vegetable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import procedures.SaladProcedures;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Created by Босс on 18.12.2016.
 */
public class SaladProceduresTest {

    private static SaladProcedures saladProcedures;
    private static FileWork fileWork;
    private static List<Vegetable> listVegetables;

    @BeforeClass
    public static void init() throws FileNotFoundException, UnsupportedEncodingException {
       saladProcedures = new SaladProcedures();
       fileWork = new FileWork("src/resources/file.txt");
    }

    @BeforeClass
    public static void readInfoFromFileTest() throws IOException {
        listVegetables = fileWork.readFromFile();
        Assert.assertNotNull(listVegetables);
    }

    @Test
    public void makeSaladTest() throws IOException {
        Chef chef = new Chef();
        Salad salad = chef.makeSalad("Весенний", listVegetables);
        Assert.assertNotEquals(salad.getVegetableList().size(), 0);
    }

    @Test
    public void countCaloriesTest(){
        Chef chef = new Chef();
        Salad salad = chef.makeSalad("Весенний", listVegetables);
        int calories = saladProcedures.countCalories(salad);
        Assert.assertTrue(calories > 1);
    }

    @Test
    public void findVegetablesTest(){
        Chef chef = new Chef();
        Salad salad = chef.makeSalad("Весенний", listVegetables);
        List<Vegetable> list = saladProcedures.findVegetables(salad, 11, 32);
        Assert.assertNotEquals(list.size(), 0);
    }

    @Test
    public void sortByNameTest(){
        Chef chef = new Chef();
        Salad sortedSalad = chef.makeSalad("Весенний", listVegetables);
        Salad salad = chef.makeSalad("Весенний", listVegetables);
        saladProcedures.sortByName(sortedSalad);
        Assert.assertNotEquals(sortedSalad, salad);
    }

    @Test
    public void sortByNameCaloriesTest(){
        Chef chef = new Chef();
        Salad sortedSalad = chef.makeSalad("Весенний", listVegetables);
        Salad salad = chef.makeSalad("Весенний", listVegetables);
        saladProcedures.sortByNameCalories(sortedSalad);
        Assert.assertNotEquals(sortedSalad, salad);
    }

}
