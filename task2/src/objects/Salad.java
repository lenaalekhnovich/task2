package objects;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Босс on 18.12.2016.
 */
public class Salad {
    private String name;
    private List<Vegetable> vegetableList;


    public Salad(String name){
        vegetableList = new ArrayList<Vegetable>();
        this.name = name;
    }

    public void setIngredient(Vegetable vegetable){
        vegetableList.add(vegetable);
    }

    public void setVegetableList(List<Vegetable> vegetableList) {
        this.vegetableList = vegetableList;
    }

    public List<Vegetable> getVegetableList() {
        return vegetableList;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Salad)) return false;

        Salad salad = (Salad) o;

        if (!getName().equals(salad.getName())) return false;
        for(int i = 0; i < getVegetableList().size(); i++){
            if(!getVegetableList().get(i).equals(salad.getVegetableList().get(i)))
                return false;
        }
        return true;

    }

    @Override
    public int hashCode() {
        int result = getName().hashCode();
        result = 31 * result + getVegetableList().hashCode();
        return result;
    }
}
