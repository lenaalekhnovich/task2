package objects;


import static procedures.SaladProcedures.checkInt;

/**
 * Created by Босс on 18.12.2016.
 */
public abstract class Vegetable {

    protected String name;
    private int calories;
    private int weight;

    Vegetable(){}
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    abstract String preparation();

    public void toVegetable(String strVegetable){
        String str;
            int index[] = new int[3];
            for (int i = 0; i < 2; i++) {
                if (i != 0)
                    index[i] = strVegetable.indexOf(' ', index[i - 1] + 1);
                else
                    index[i] = strVegetable.indexOf(' ');
            }
            str = strVegetable.substring(0, index[0]);
            this.setName(str);

            str = new String(strVegetable.substring(index[0] + 1, index[1]));
            if (checkInt(str))
                this.setCalories(Integer.parseInt(str));
            else
                this.setCalories(0);

            str = strVegetable.substring(index[1] + 1);
            if (checkInt(str))
                this.setWeight(Integer.parseInt(str));
            else
                this.setCalories(0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vegetable)) return false;

        Vegetable vegetable = (Vegetable) o;

        if (getCalories() != vegetable.getCalories()) return false;
        if (weight != vegetable.weight) return false;
        return getName().equals(vegetable.getName());

    }

    @Override
    public int hashCode() {
        int result = getName().hashCode();
        result = 31 * result + getCalories();
        result = 31 * result + weight;
        return result;
    }
}
