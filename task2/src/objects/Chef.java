package objects;

import java.util.List;

/**
 * Created by Босс on 18.12.2016.
 */
public class Chef {

    public Salad makeSalad(String name, List<Vegetable> listIngredients){
        Salad salad = new Salad(name);
        for(int i = 0; i < listIngredients.size(); i++){
            salad.setIngredient(listIngredients.get(i));
        }
        return salad;
    }
}
