package procedures;

import exception.IntegerValueException;
import objects.Salad;
import objects.Vegetable;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Босс on 18.12.2016.
 */
public class SaladProcedures {

    static Logger logger = Logger.getLogger(SaladProcedures.class);

    public static boolean checkInt(String str){
        int number;
        try {

            number = Integer.parseInt(str);
            if(number == 0)
                throw new IntegerValueException("Значение массы/калорий не может быть 0");
        } catch (IntegerValueException e) {
            logger.error("negative argument: ", e);
           return false;
        } catch(Exception e){
            logger.error("negative argument:", e);
            return false;
        }
        return true;
    }
    
    public int countCalories(Salad salad){
        int calories = 0;
        for(int i = 0; i < salad.getVegetableList().size(); i++) {
            calories += salad.getVegetableList().get(i).getCalories();
        }
        return calories;
    }
    
    public List<Vegetable> findVegetables(Salad salad, int beginCalorie, int endCalorie){
        List<Vegetable> list = new ArrayList<>();
        Vegetable vegetable;
        for(int i = 0; i < salad.getVegetableList().size(); i++){
            vegetable = salad.getVegetableList().get(i);
            if( vegetable.getCalories() > beginCalorie || vegetable.getCalories() < endCalorie)
                list.add(vegetable);
        }
        return list;
    }

    public Salad sortByName(Salad salad) {
        List<Vegetable> list = salad.getVegetableList();
        Collections.sort(list, new Comparator<Vegetable>() {
            public int compare(Vegetable o1, Vegetable o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        salad.setVegetableList(list);
        return salad;
    }

    public Salad sortByNameCalories(Salad salad){
        List<Vegetable> list = salad.getVegetableList();
        Collections.sort(list, new SortByNameCalories());
        salad.setVegetableList(list);
        return salad;
    }
}
