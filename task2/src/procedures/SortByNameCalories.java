package procedures;

import objects.Vegetable;

import java.util.Comparator;

/**
 * Created by Босс on 18.12.2016.
 */
public class SortByNameCalories implements Comparator<Vegetable> {

    @Override
    public int compare(Vegetable o1, Vegetable o2) {
        int result = o1.getName().compareTo(o2.getName());
        if (result != 0)
            return (int)(result/Math.abs(result));
        result = Integer.toString(o1.getCalories()).compareTo(Integer.toString(o2.getCalories()));
        return (result != 0) ? (int)(result/Math.abs(result)) : 0;
    }
}
