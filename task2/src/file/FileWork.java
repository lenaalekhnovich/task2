package file;

import objects.FruitedVegetable;
import objects.LeafyVegetable;
import objects.RootVegetable;
import objects.Vegetable;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Босс on 18.12.2016.
 */
public class FileWork {
    private FileInputStream inF;
    private BufferedReader reader;

    static Logger logger = Logger.getLogger(FileWork.class);

    public FileWork(String fileName) throws FileNotFoundException, UnsupportedEncodingException {
        inF = new FileInputStream(fileName);
        reader = new BufferedReader(new InputStreamReader(inF, "UTF-8"));
    }

    public List<Vegetable> readFromFile() throws IOException {
        List<Vegetable> list = new ArrayList<>();
        try {
            String str;
            str=reader.readLine();
            Vegetable rootVeg = new RootVegetable();
            rootVeg.toVegetable(str);
            list.add(rootVeg);

            str=reader.readLine();
            Vegetable leafVeg = new LeafyVegetable();
            leafVeg.toVegetable(str);
            list.add(leafVeg);

            str=reader.readLine();
            rootVeg =  new RootVegetable();
            rootVeg.toVegetable(str);
            list.add(rootVeg);

            str=reader.readLine();
            FruitedVegetable fruitedVeg = new FruitedVegetable();
            fruitedVeg.toVegetable(str);
            list.add(fruitedVeg);
        } catch(IOException ex){
            logger.error("negative argument: ", ex);
            return null;
        }
        return list;
    }


}
