package exception;

/**
 * Created by Босс on 18.12.2016.
 */
public class IntegerValueException extends Exception {

    public IntegerValueException(String message){
        super(message);
    }
}
